#!/usr/bin/env python3
#
# # # # # #
class pixelpoachers_game:
	def __init__(self):
		# # # #
		# Misc game variables
		# # # #
		self.mob_list = []
		self.winning_score = random.randint(420,1312)
		# # # #
		# Create the main Tkinter window
		# # # #
		self.game_window = tkinter.Tk()
		self.game_window.title("PixelPoacher")
		self.game_window.geometry("500x500")
		self.game_window.resizable(False, False)
		# # # #
		# Creates the canvas in the main window
		# # # #
		self.game_canvas = tkinter.Canvas(self.game_window, width=490, height=490, bg="black", bd=0)
		self.game_canvas.pack()
		# # # #
		# Create the player object
		# # # #
		self.main_player = pixelpoachers_player(self)
		# # # #
		# Create the enemy mobs
		# # # #
		mob_total = random.randint(75,150)
		mob_count = 0
		while mob_total > mob_count:
			new_mob = pixelpoachers_mob(self)
			self.mob_list.append(new_mob)
			mob_count += 1
		# # # #
		# Handle the music
		# # # #
		self.music_player()
		# # # #
		# Main loop of the game
		# # # #
		self.game_window.bind_all('<Key>', self.main_logic)
		self.game_window.mainloop()
	
	def main_logic(self, event):
		try:
			# # # #
			# Gets input from the player
			# # # #
			if ((event.keysym) == 'Up' or (event.keysym == 'Down') or (event.keysym == 'Left') or (event.keysym == 'Right') or (event.keysym == 'Escape')):
				# # # #
				# Executes the player input
				# # # #
				if event.keysym == 'Up':
					if self.game_canvas.coords(self.main_player.player_icon)[1] > 5.0:
						self.game_canvas.move(self.main_player.player_icon, 0, -5)
				elif event.keysym == 'Down':
					if self.game_canvas.coords(self.main_player.player_icon)[1] < 485.0:
						self.game_canvas.move(self.main_player.player_icon, 0, 5)
				elif event.keysym == 'Left':
					if self.game_canvas.coords(self.main_player.player_icon)[0] > 5.0:
						self.game_canvas.move(self.main_player.player_icon, -5, 0)
				elif event.keysym == 'Right':
					if self.game_canvas.coords(self.main_player.player_icon)[0] < 485.0:
						self.game_canvas.move(self.main_player.player_icon, 5, 0)
				elif event.keysym == 'Escape':
					self.close_game('exited')
				# # # #
				# Let the mobs make their move
				# # # #
				try:
					for mob in self.mob_list:
						mob.mob_logic()
				except:
					pass
			# # # #
			# If the entry isn't valid, do nothing
			# # # #
			else:
				pass
		except:
			pass
		# # # #
		# Checks state of the game (score, death, etc.)
		# # # #
		try:
			self.state_check()
		except:
			pass
		#
		return 0
	#
	def music_player(self):
		self.pygame = pygame
		self.pygame.init()
		self.pygame.mixer.init()
		self.pygame.mixer.music.load('pixelpoacher_soundtrack.wav')
		self.pygame.mixer.music.play(-1)
		return 0
	#
	def state_check(self):
		# # # #
		# Is the player dead?
		# # # #
		if len(self.game_canvas.find_overlapping(self.game_canvas.coords(self.main_player.player_icon)[0], self.game_canvas.coords(self.main_player.player_icon)[1], self.game_canvas.coords(self.main_player.player_icon)[2], self.game_canvas.coords(self.main_player.player_icon)[3])) > 1:
			self.close_game('died')
		else:
			self.main_player.score += 1
		# # # #
		# Has the player won yet?
		# # # #
		if self.main_player.score >= self.winning_score:
			self.close_game('won')
		#
		return 0
	#
	def close_game(self, reason):
		# # # #
		# Pauses the music and destroys the game canvas
		# # # #
		self.game_canvas.delete("all")
		self.game_canvas.destroy()
		self.pygame.mixer.pause()
		self.pygame.mixer.quit()
		# # # #
		# If the player died
		# # # #
		if reason == "died":
			game_over = tkinter.Label(self.game_window, text="GAME OVER. YOU DIED!", font='Helvetica 24 bold')
			game_over.pack()
			final_score = tkinter.Label(self.game_window, text="Final Score: " + str(self.main_player.score) + "\n\n\n\n\n")
			final_score.pack()
		# # # #
		# If the player exited
		# # # #
		elif reason == "exited":
			game_over = tkinter.Label(self.game_window, text="GAME OVER. YOU QUIT!", font='Helvetica 24 bold')
			game_over.pack()
			final_score = tkinter.Label(self.game_window, text="Final Score: " + str(self.main_player.score) + "\n\n\n\n\n")
			final_score.pack()
		# # # #
		# If the player won
		# # # #
		elif reason == "won":
			game_over = tkinter.Label(self.game_window, text="GAME OVER. YOU WON!", font='Helvetica 24 bold')
			game_over.pack()
			final_score = tkinter.Label(self.game_window, text="Final Score: " + str(self.main_player.score) + "\n\n")
			final_score.pack()
			download_prize = tkinter.Button(self.game_window, width = 50, height = 2, text = "Download Your Prize")
			download_prize.pack()
			download_prize.bind("<Button-1>", lambda e: webbrowser.open_new("https://toxitolerant.com"))
		# # # #
		# Prints the credits
		# # # #
		close_button = tkinter.Button(self.game_window, width = 50, height = 2, text = "Exit", command = lambda: self.exit_game())
		close_button.pack()
		restart_button = tkinter.Button(self.game_window, width = 50, height = 2, text = "Restart", command = lambda: self.restart_game())
		restart_button.pack()
		blank_space = tkinter.Label(self.game_window, text="\n\n")
		blank_space.pack()
		credit_title = tkinter.Label(self.game_window, text="PixelPoacher Credits:", font='Helvetica 18 bold')
		credit_title.pack()
		game_title = tkinter.Label(self.game_window, text="Development", font='Helvetica 12 bold')
		game_title.pack()
		game_credits_text = """Copyright (c) 2020, Chris Blankenship 
cblankenship@pm.me • chrisblankenship.dev
		"""
		game_credits_display = tkinter.Label(self.game_window, text=game_credits_text)
		game_credits_display.pack()
		music_title = tkinter.Label(self.game_window, text="Music", font='Helvetica 12 bold')
		music_title.pack()
		music_credits_text = """Copyright (c) 2020, Toxitolerant
hello@toxitolerant.com • toxitolerant.com

Musical adaptations by Chris Blankenship
cblankenship@pm.me • chrisblankenship.dev
		"""
		music_credits_display = tkinter.Label(self.game_window, text=music_credits_text)
		music_credits_display.pack()
		return 0
	#
	def exit_game(self):
		# # # #
		# Deletes the objects
		# # # #
		self.game_window.destroy()
		for mob in self.mob_list:
			del(mob)
		del(self.main_player)
		del(self)
		return 0
	#
	def restart_game(self):
		# # # #
		# Exits the game and creates a new game object
		# # # #
		self.exit_game()
		pixelpoachers_game()
		return 0
# # # # # #
# 
# # # # # #
class pixelpoachers_player():
	def __init__(self, game_instance):
		# # # #
		# Connect player to game object
		# # # #
		self.game_instance = game_instance
		# # # #
		# Store basic info about the player
		# # # #
		self.position_x = random.randrange(5,480,5)
		self.position_y = random.randrange(5,480,5)
		self.score = 0
		# # # #
		# Create player icon
		# # # #
		self.player_icon = self.game_instance.game_canvas.create_rectangle(self.position_x, self.position_y, self.position_x + 5, self.position_y + 5, fill="cyan")
# # # # # #
# 
# # # # # #
class pixelpoachers_mob():
	def __init__(self, game_instance):
		# # # #
		# Connect object to game object
		# # # #
		self.game_instance = game_instance
		# # # #
		# Generate the spawn point for the mob, but not too close to the player
		# # # #
		maybe_x = random.randrange(10,480,5)
		maybe_y = random.randrange(10,480,5)
		while ((self.game_instance.main_player.position_x - 5) <= maybe_x <= (self.game_instance.main_player.position_x + 5)) and ((self.game_instance.main_player.position_y - 5) <= maybe_y <= (self.game_instance.main_player.position_y + 5)):
			maybe_x = random.randrange(10,480,5)
			maybe_y = random.randrange(10,480,5)
		self.position_x = maybe_x
		self.position_y = maybe_y
		#
		self.mob_icon = self.game_instance.game_canvas.create_rectangle(self.position_x, self.position_y, self.position_x + 5, self.position_y + 5, fill="red")
	#
	def mob_logic(self):
		decision = random.randint(0,1)
		# # # #
		# Most of the time the moob will move closer to the player
		# # # #
		if decision == 0:
			if self.game_instance.game_canvas.coords(self.mob_icon)[1] >= 5.0 and self.game_instance.game_canvas.coords(self.mob_icon)[0] < 485.0:
				closer_x = abs(self.game_instance.game_canvas.coords(self.mob_icon)[0] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[0])
				closer_y = abs(self.game_instance.game_canvas.coords(self.mob_icon)[1] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[1])
				# # # #
				# If they are closer on the X axis. . .
				# # # #
				if closer_x > closer_y:
					if (self.game_instance.game_canvas.coords(self.mob_icon)[0] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[0]) >= 0:
						if (self.game_instance.game_canvas.coords(self.mob_icon)[0] < 485.0) and (self.game_instance.game_canvas.coords(self.mob_icon)[0] - 5.0 >= 5.0):
							self.game_instance.game_canvas.move(self.mob_icon, -5, 0)
						else:
							self.game_instance.game_canvas.move(self.mob_icon, 5, 0)
					elif (self.game_instance.game_canvas.coords(self.mob_icon)[0] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[0]) < 0:
						if (self.game_instance.game_canvas.coords(self.mob_icon)[0] >= 5.0) and (self.game_instance.game_canvas.coords(self.mob_icon)[0] + 5.0 < 485.0):
							self.game_instance.game_canvas.move(self.mob_icon, 5, 0)
						else:
							self.game_instance.game_canvas.move(self.mob_icon, -5, 0)
				# # # #
				# If they are closer on the Y axis. . .
				# # # #
				elif closer_x < closer_y:
					if (self.game_instance.game_canvas.coords(self.mob_icon)[1] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[1]) >= 0:
						if (self.game_instance.game_canvas.coords(self.mob_icon)[1] < 485.0) and (self.game_instance.game_canvas.coords(self.mob_icon)[1] - 5.0 >= 5.0):
							self.game_instance.game_canvas.move(self.mob_icon, 0, -5)
						else:
							self.game_instance.game_canvas.move(self.mob_icon, 0, 5)
					elif (self.game_instance.game_canvas.coords(self.mob_icon)[1] - self.game_instance.game_canvas.coords(self.game_instance.main_player.player_icon)[1]) < 0:
						if (self.game_instance.game_canvas.coords(self.mob_icon)[1] >= 5.0) and (self.game_instance.game_canvas.coords(self.mob_icon)[1] + 5.0 < 485.0):
							self.game_instance.game_canvas.move(self.mob_icon, 0, 5)
						else:
							self.game_instance.game_canvas.move(self.mob_icon, 0, -5)
		# # # #
		# Sometimes the mob will move randomly
		# # # #
		elif decision == 1:
			movement = random.randint(0,3)
			if movement == 0: # Up
				if self.game_instance.game_canvas.coords(self.mob_icon)[1] > 5.0:
					self.game_instance.game_canvas.move(self.mob_icon, 0, -5)
				else:
					self.game_instance.game_canvas.move(self.mob_icon, 0, 5)
			elif movement == 1: # Down
				if self.game_instance.game_canvas.coords(self.mob_icon)[1] < 485.0:
					self.game_instance.game_canvas.move(self.mob_icon, 0, 5)
				else:
					self.game_instance.game_canvas.move(self.mob_icon, 0, -5)
			elif movement == 2: # Left
				if self.game_instance.game_canvas.coords(self.mob_icon)[0] > 5.0:
					self.game_instance.game_canvas.move(self.mob_icon, -5, 0)
				else:
					self.game_instance.game_canvas.move(self.mob_icon, 5, 0)
			elif movement == 3: # Right
				if self.game_instance.game_canvas.coords(self.mob_icon)[0] < 485.0:
					self.game_instance.game_canvas.move(self.mob_icon, 5, 0)
				else:
					self.game_instance.game_canvas.move(self.mob_icon, -5, 0)
		return 0
# # # # # #
# 
# # # # # #
def main(args):
	# # # #
	# Creates a game object
	# # # #
	pixelpoachers_game()
	return 0
# # # # # #
#
# # # # # #
if __name__ == '__main__':
	import os, sys
	with open(os.devnull, 'w') as dev_null:
		old_stdout = sys.stdout
		sys.stdout = dev_null
		import pygame, random, tkinter, webbrowser
		sys.stdout = old_stdout
	sys.exit(main(sys.argv))
# # # # # #
